# Changelog

All notable changes to `ch-feedback` will be documented in this file

## 1.0.0 - 201X-XX-XX

- initial release

##2.0.0
- update controller to extend core controllers
- moved plugin logic inside src
- publish only config, routes and the views