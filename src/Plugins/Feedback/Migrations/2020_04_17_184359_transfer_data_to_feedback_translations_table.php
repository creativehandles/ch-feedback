<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferDataToFeedbackTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move the existing data into the translations table
        DB::statement(
            'insert into feedback_translations (feedback_id, locale, text) select id, :locale, text from feedbacks',
            ['locale' => config('app.locale')])
        ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update feedbacks as m join feedback_translations as t on m.id = t.feedback_id set m.text = t.text where t.locale = :locale',
            ['locale' => config('app.locale')])
        ;
    }
}
