<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 24/7/19
 * Time: 2:19 PM
 */

namespace Creativehandles\ChFeedback\Plugins\Feedback;


use Creativehandles\ChFeedback\Plugins\Feedback\Repositories\FeedbackGroupRepository;
use Creativehandles\ChFeedback\Plugins\Feedback\Repositories\FeedbackRepository;
use Creativehandles\ChFeedback\Plugins\Plugin;

class Feedback extends Plugin
{

    /**
     * @var FeedbackRepository
     */
    private $feedbackRepository;
    /**
     * @var FeedbackGroupRepository
     */
    private $feedbackGroupRepository;

    public function __construct(FeedbackRepository $feedbackRepository,FeedbackGroupRepository $feedbackGroupRepository)
    {
        parent::__construct();

        $this->feedbackRepository = $feedbackRepository;
        $this->feedbackGroupRepository = $feedbackGroupRepository;
    }

    public function getAllFeedbacks()
    {
        return $this->feedbackRepository->all(['user','groups']);
    }

    public function createOrUpdateFeedback($data){
        return $this->feedbackRepository->updateOrCreate($data,'id');
    }

    public function createNewGroup($data)
    {
        return $this->feedbackGroupRepository->updateOrCreate($data,'name');
    }

    public function getAllFeedbackByVisibility($visibility=true)
    {
        return $this->feedbackRepository->findBy('visibility',(int) $visibility,['user','groups']);
    }

    public function getFeedbackByGroup($group=null)
    {
        if($group){
            return $this->feedbackGroupRepository->findBy('name',$group,['feedbacks','feedbacks.user']);
        }else{
            return $this->feedbackGroupRepository->all(['feedbacks','feedbacks.user']);
        }
    }

    public function getFeedbackById($id)
    {
        return $this->feedbackRepository->find($id,['groups','user']);
    }

}
