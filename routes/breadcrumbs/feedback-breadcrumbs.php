<?php

// Dashboard > feedback
Breadcrumbs::for('admin.feedback', function ($trail) {
//    $trail->parent('dashboard');
    $trail->push(__('general.Feedbacks'), route('admin.feedback'));
});

// Dashboard > trainings > create
Breadcrumbs::for('admin.feedback.create', function ($trail) {
    $trail->parent('admin.feedback');
    $trail->push(__('general.Create Feedback'), route('admin.feedback.create'));
});

// Dashboard > trainings > edit
Breadcrumbs::for('admin.feedback.edit', function ($trail, $feedback) {
    $trail->parent('admin.feedback');
    $trail->push(__('general.Edit Feedback'), route('admin.feedback.edit', 1));
});
